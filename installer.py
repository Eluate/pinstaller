'''
Description: Handles the installation of packages

Created: 16 Dec 2014
Last Modified: 29 Dec 2014
'''

import os
from subprocess import Popen, PIPE, STDOUT
import zipfile
from uuid import uuid4

error_messages = ["./configure: not found", "No targets specified and no makefile found.  Stop.",
                  "getcwd() failed: No such file or directory", "No targets specified and no makefile found.  Stop.",
                  "no tty present and no askpass program specified", "No rule to make target 'install'.  Stop."]

def handle_install(self, path):
    # Check if installing a file or folder
    if os.path.exists(path):
        if os.path.isdir(path):
            folder_install(self, path)
        elif os.path.isfile(path):
            file_install(self, path)
        else:
            if self == "Terminal":
                os.system("echo Path doesn't exist")
            else:
                self.txt_file.setText("Path doesn't exist")
    else:
        if self == "Terminal":
            os.system("echo Path doesn't exist")
        else:
            self.txt_file.setText("Path doesn't exist")
        
    
def file_install(self, path):
    # Create a temporary installation folder
    temp_folder = str(os.path.dirname(path) + "/" + str(uuid4()) + "/")
    os.makedirs(temp_folder)
    formatted_path = path.lower().strip().replace("'", "").replace('"', '')
    
    # Check if the file is tar or tar compatible
    if (formatted_path.endswith(".tar") or formatted_path.endswith(".tar.gz") or
        formatted_path.endswith(".tgz") or formatted_path.endswith(".tar.xz") or 
        formatted_path.endswith(".tar.bz2") or formatted_path.endswith(".tar.tbz2") or
        formatted_path.endswith(".tar.lzma") or formatted_path.endswith(".tlz")):
        working_path = os.path.dirname(path)
        extract_process = Popen("tar -xvf " + os.path.basename(path) + " -C " + os.path.basename(os.path.normpath(temp_folder)), stdout = PIPE, stderr = STDOUT, shell = True, cwd = working_path)
        for line in iter(extract_process.stdout.readline, ""):
            self.txt_installoutput.insertPlainText("Extracting: " + line)
        self.txt_installoutput.insertPlainText("Extraction complete \n")
    # Check if the file is a zip file
    elif (formatted_path.endswith(".zip")):
        os.chdir(os.path.dirname(path))
        with zipfile.ZipFile(os.path.basename(path), "r") as z:
            z.extractall(os.path.basename(os.path.normpath(temp_folder)))
        os.chdir("/")
    # Otherwise remove temp_folder and return an error
    else:
        if self == "terminal":
            os.system("echo Unsupported File Type")
        else:
            self.txt_file.setText("Unsupported File Type")
        return
    
    # Start Installation
    install_message = "Failed"
    
    # Loop through the temporary folder, trying to install the program inside each folder        
    for direc in os.listdir(temp_folder):
        if os.path.isdir(direc):
            install_message = install(self, temp_folder + direc)
            if install_message == "Success":
                break

    # If the program hasn't installed yet, install it in the parent folder
    if install_message == "Failed":
        install_message = install(self, temp_folder)

    # Delete temp_folder after install
    os.system("echo Deleting Temporary Folder")
    try:
        for root, dirs, files in os.walk(temp_folder, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(temp_folder)
        os.system("echo Deleted temporary folder")
        self.txt_installoutput.insertPlainText("Deleted temporary folder \n")
    except OSError:
        os.system("echo Failure deleting temporary folder")
        self.txt_installoutput.insertPlainText("Failure deleting temporary folder \n")
    
    if install_message == "Failed":
        os.system("echo Installation process finished with errors")
        self.txt_installoutput.insertPlainText("Installation process finished with errors \n")
    else:
        os.system("echo Installation process succeeded")
        self.txt_installoutput.insertPlainText("Installation process succeeded \n")
        
    # TODO Open Dialog for success or failure of installation
    
def folder_install(self, path):
    install_message = install(self, path)
    
    if install_message == "Failed":
        os.system("echo Installation process finished with errors")
        self.txt_installoutput.insertPlainText("Installation process finished with errors \n")
    else:
        os.system("echo Installation process succeeded")
        self.txt_installoutput.insertPlainText("Installation process succeeded \n")
    
def install(self, path):
    self.txt_installoutput.insertPlainText("Installation starting \n")
    try:
        # Install Method 1
        working_path = os.path.dirname(path)
        terminal_process = Popen("./configure; make; make install", cwd = working_path, stdout = PIPE, stderr = STDOUT, shell = True)
        for line in iter(terminal_process.stdout.readline, ""):
            self.txt_installoutput.insertPlainText(line + "\n")
        os.chdir("/")    
        
        # Handle Errors
        for eacherror in error_messages:
            if eacherror in self.txt_installoutput.toPlainText():
                return "Failed"
        return "Success"
    except OSError, e:
        self.txt_installoutput.insertPlainText(str(e) + "\n")
        os.chdir("/")
        return "Failed"
    os.chdir("/")
    return "Failed"
