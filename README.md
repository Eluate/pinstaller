# Pinstaller

Simplifies the installation of packages on debian based linux systems.

## Requirements:
* Python 2.7+ OR Python 3.2+
* [PyQt4](http://www.riverbankcomputing.com/software/pyqt/download)

## How to use:
* Run `sudo -s` in the terminal
* Run `python path/to/handler.py` to open the program with the GUI
* Run `python path/to/handler.py path/to/folder_or_file_to_install` to run the program through the terminal 

## License:
Pinstaller is licensed under the MIT License. The license can be viewed in `LICENSE`.

## Authors:
* [Harold Seefeld](https://github.com/Zaphron)
