'''
Description: Handles the main program

Created: 16 Dec 2014
Last Modified: 17 Dec 2014
'''

import os
import sys
import ui

from PyQt4 import QtGui
from installer import handle_install

# Check if the program has been run through the command line
if len(sys.argv) == 1:
    uiapp = QtGui.QApplication(sys.argv)
    dialog = QtGui.QDialog()
    ui.Ui_Dialog().setup_ui(dialog)
    dialog.show()
    sys.exit(uiapp.exec_())
elif len(sys.argv) == 2:
    if sys.argv[1].upper() == "HELP":
        os.system("echo Usage - 'pinstall <File Path>'")
    elif os.path.exists(sys.argv[1]):
        handle_install(sys.argv)
else:
    os.system("echo Usage - 'pinstall <File Path>'")
    
    
