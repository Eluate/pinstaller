'''
Description: Handles the user interface

Created: 16 Dec 2014
Last Modified: 25 Dec 2014
'''

from PyQt4 import QtCore, QtGui
from installer import handle_install
from PyQt4.QtGui import QFileDialog

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setup_ui(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(480, 310)
        Dialog.setMinimumSize(QtCore.QSize(480, 310))
        Dialog.setMaximumSize(QtCore.QSize(480, 310))
        
        self.btn_choosefile = QtGui.QPushButton(Dialog)
        self.btn_choosefile.setGeometry(QtCore.QRect(10, 15, 120, 30))
        self.btn_choosefile.setObjectName(_fromUtf8("btn_choosefile"))
        
        self.btn_choosefolder = QtGui.QPushButton(Dialog)
        self.btn_choosefolder.setGeometry(QtCore.QRect(140, 15, 120, 30))
        self.btn_choosefolder.setObjectName(_fromUtf8("btn_choosefolder"))
        
        self.txt_file = QtGui.QTextEdit(Dialog)
        self.txt_file.setGeometry(QtCore.QRect(10, 55, 340, 30))
        self.txt_file.setReadOnly(True)
        self.txt_file.setObjectName(_fromUtf8("txt_file"))
        
        self.btn_install = QtGui.QPushButton(Dialog)
        self.btn_install.setGeometry(QtCore.QRect(370, 15, 100, 30))
        self.btn_install.setObjectName(_fromUtf8("btn_install"))
        
        self.btn_reset = QtGui.QPushButton(Dialog)
        self.btn_reset.setGeometry(QtCore.QRect(370, 55, 100, 30))
        self.btn_reset.setObjectName(_fromUtf8("btn_reset"))
        
        self.txt_installoutput = QtGui.QTextEdit(Dialog)
        self.txt_installoutput.setGeometry(QtCore.QRect(10, 140, 460, 150))
        self.txt_installoutput.setReadOnly(True)
        self.txt_installoutput.setObjectName(_fromUtf8("txt_installoutput"))
        
        self.lbl_Output = QtGui.QLabel(Dialog)
        self.lbl_Output.setGeometry(QtCore.QRect(10, 120, 130, 20))
        self.lbl_Output.setObjectName(_fromUtf8("lbl_Output"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        
        self.btn_choosefile.clicked.connect(lambda: self.select_file())
        self.btn_choosefolder.clicked.connect(lambda: self.select_folder())
        self.btn_reset.clicked.connect(lambda: self.reset_file())
        self.btn_install.clicked.connect(lambda: self.install_file())
        
    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Pinstaller", None))
        self.btn_choosefile.setText(_translate("Dialog", "Choose File", None))
        self.btn_choosefolder.setText(_translate("Dialog", "Choose Folder", None))
        self.txt_file.setText(_translate("Dialog", "No File or Folder selected", None))
        self.btn_install.setText(_translate("Dialog", "Install", None))
        self.btn_reset.setText(_translate("Dialog", "Reset", None))
        self.lbl_Output.setText(_translate("Dialog", "Installation Output", None))
        
    def select_file(self):
        self.txt_file.setText(QFileDialog.getOpenFileName())
        
    def select_folder(self):
        self.txt_file.setText(QFileDialog.getExistingDirectory())
    
    def reset_file(self):
        self.txt_file.setText("No File or Folder selected")
        self.txt_installoutput.setText("")
    
    def install_file(self):
        self.txt_installoutput.setText("")
        handle_install(self, str(self.txt_file.toPlainText()))
        self.txt_installoutput.moveCursor(QtGui.QTextCursor.End)
